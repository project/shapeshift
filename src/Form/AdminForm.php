<?php
/**
* @file
* Contains \Drupal\shapeshift\Form\AdminForm.
*/

namespace Drupal\shapeshift\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
* Defines a form to configure maintenance settings for this site.
*/
class AdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'shapeshift_admin';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['shapeshift.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = \Drupal::config('shapeshift.settings');

    $form['public_key'] = [
      '#type' => 'textfield',
      '#title' => t("Public key"),
      '#default_value' => $config->get('public_key'),
    ];

    $form['private_key'] = [
      '#type' => 'textfield',
      '#title' => t("Private key"),
      '#default_value' => $config->get('private_key'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::configFactory()->getEditable('shapeshift.settings');
    $config->set('public_key', $form_state->getValue('public_key'));
    $config->set('private_key', $form_state->getValue('private_key'));
    $config->save();

    parent::submitForm($form, $form_state);
  }
}
