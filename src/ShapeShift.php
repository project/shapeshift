<?php

/**
 * @file
 * Contains \Drupal\shapeshift\ShapeShift.
 */

namespace Drupal\shapeshift;

use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Url;
use Drupal\Core\Template\Attribute;
use Drupal\cointools\CoinTools;

class ShapeShift {

  public static function button($address, $amount) {
    $query = [
      'destination' => $address,
      'amount' => CoinTools::satoshiToBitcoin($amount),
    ];

    $key = \Drupal::config('shapeshift.settings')->get('public_key');

    if ($key != '') {
      $query['apiKey'] = $key;
    }

    $url = Url::fromUri('https://shapeshift.io/shifty.html', ['query' => $query]);
    $module_path = \Drupal::moduleHandler()->getModule('shapeshift')->getPath();

    $attributes = [
      'src' => Url::fromUri('base:' . $module_path . '/shifty-button.png')->toString(),
      'width' => 287,
      'height' => 65,
      'alt' => t("Pay with Altcoins"),
      'style' => 'margin-top: 20px; width: 200px; height: auto;'
    ];
    $img = '<img' . new Attribute($attributes) . ' />';

    return SafeMarkup::set('<a href="' . $url->toString() . '" target="_blank">' . $img . '</a>');
  }

}
