<?php

/**
 * @file
 * Contains \Drupal\shapeshift\Controller\ShapeShiftController.
 */

namespace Drupal\shapeshift\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Component\Utility\SafeMarkup;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\cointools\CoinTools;

/**
 * Controller routines for Coin ShapeShift routes.
 */
class ShapeShiftController extends ControllerBase {

  public function coins() {
    $client = \Drupal::httpClient();
    $response = $client->get('https://shapeshift.io/getcoins');

    foreach ($response->json() as $coin) {
      $rows[] = [
        SafeMarkup::set('<img src="' . $coin['image'] . '" style="width: 50px; height: 50px;" />'),
        $coin['name'],
        [
          'data' => $coin['symbol'],
          'class' => ['cointools-monospace'],
        ],
        $coin['status'],
      ];
    }

    return [
      '#theme' => 'table',
      '#header' => [
        t("Logo"),
        t("Name"),
        t("Code"),
        t("Status"),
      ],
      '#rows' => $rows,
    ];
  }

  public function transactions() {
    $client = \Drupal::httpClient();
    $key = \Drupal::config('shapeshift.settings')->get('private_key');
    $response = $client->get('https://shapeshift.io/txbyapikey/' . $key);

    foreach ($response->json() as $txn) {
      $txn += [
        'inputAmount' => '',
        'inputCurrency' => '',
        'outputAmount' => '',
        'outputCurrency' => '',
        'shiftRate' => '',
      ];
      $rows[] = [
        [
          'data' => $txn['inputAmount'],
          'style' => 'text-align: right;',
        ],
        [
          'data' => $txn['inputCurrency'],
          'class' => ['cointools-monospace'],
        ],
        [
          'data' => $txn['outputAmount'],
          'style' => 'text-align: right;',
        ],
        [
          'data' => $txn['outputCurrency'],
          'class' => ['cointools-monospace'],
        ],
        [
          'data' => $txn['shiftRate'],
          'style' => 'text-align: right;',
        ],
        $txn['status'],
      ];
    }

    return [
      '#theme' => 'table',
      '#header' => [
        [
          'data' => t("In"),
          'style' => 'text-align: right;',
        ],
        '',
        [
          'data' => t("Out"),
          'style' => 'text-align: right;',
        ],
        '',
        [
          'data' => t("Shift Rate"),
          'style' => 'text-align: right;',
        ],
        t("Status"),
      ],
      '#rows' => $rows,
    ];
  }

}
